<?php


class Controller extends CI_Controller
{
    /**
     * Render view
     * @param string $view
     * @param array $data
     */
    protected function render($view, $data = [])
    {
        $this->set_active_item($this->current_page);
        $content = $this->load->view($view, $data, true);
        $this->load->view('layouts/main', ['content' => $content, 'menu' => $this->menu]);
    }
} 