<?php


/**
 * Class Account_model
 * @property CI_DB_Active_record $db
 */
class Account_model extends CI_Model
{
    const SERVICE_ACCOUNT = 0;
    const SERVICE_FEE = 0.0099;


    public $client;
    public $serial;
    public $balance;

    function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    /**
     * Get service account
     * @return mixed
     */
    public function get_service_account()
    {
        $query = $this->db->get_where('account', ['serial' => self::SERVICE_ACCOUNT]);
        return $query->row();
    }

    /**
     * Get all accounts excluding service account
     * @return mixed
     */
    public function get_all()
    {
        $query = $this->db
            ->from('account')
            ->where('serial !=', self::SERVICE_ACCOUNT)
            ->order_by('serial DESC')
            ->get();

        return $query->result();
    }

    /**
     * Get account by id
     * @param $id
     */
    public function get($id)
    {
        $query = $this->db
            ->from('account')
            ->where('id', $id)
            ->get();

        return $query->row();
    }

    /**
     * Get all account transfers
     * @param int $id
     * @param int $page
     * @return array
     */
    public function get_transfers($id, $page = 1)
    {
        $page_size = 10;
        $offset = ($page - 1) * 10;

        $query = $this->db
            ->select('id')
            ->where('from_id', $id)
            ->or_where('to_id', $id)
            ->limit($page_size)
            ->offset($offset)
            ->order_by('time DESC')
            ->get('transfer');

        $transfers = $query->result('array');

        // only for php >= 5.5
        $ids = array_column($transfers, 'id');

        if(empty($ids)) {
            return [];
        }

        $query = $this->db
            ->select('transfer.*, account.*, account.id as account')
            ->where_in('transfer.id', $ids)
            ->join('account', 'account.id = transfer.from_id OR account.id = transfer.to_id', 'LEFT')
            ->order_by('transfer.time DESC')
            ->get('transfer');


        $result = $query->result();

        $result = array_reduce($result, function ($carry, $transfer) use ($id) {
                if ($id === $transfer->account) {
                    return $carry;
                }

                if ($id === $transfer->from_id) {
                    $carry[] = (object)[
                        'time' => $transfer->time,
                        'account' => $transfer->to_id,
                        'serial' => $transfer->serial,
                        'income' => '',
                        'withdraw' => $transfer->amount,
                        'balance' => $transfer->from_balance
                    ];
                } else {
                    $carry[] = (object)[
                        'time' => $transfer->time,
                        'account' => $transfer->from_id,
                        'serial' => $transfer->serial,
                        'income' => $transfer->amount,
                        'withdraw' => '',
                        'balance' => $transfer->to_balance
                    ];
                }
                return $carry;
            });

        return $result;
    }

    public function insert()
    {
        $this->db->insert('account', $this);
    }

    public function update($id, $options = [])
    {
        if(empty($options)) {
            $options = $this;
        }

        $this->db->update('account', $options, ['id' => $id]);
    }


    public function transfer_money($from, $to, $amount)
    {
        $from_account = $this->get($from);
        $to_account = $this->get($to);
        $service_account = $this->get_service_account();

        if (empty($from_account) || empty($to_account)) {
            throw new Exception('One of selected accounts does not exist');
        }


        $fee_amount = bcmul($amount, self::SERVICE_FEE, 2);
        $from_account->balance = bcsub($from_account->balance, $amount, 2);
        $from_account->balance = bcsub($from_account->balance, $fee_amount, 2);
        $to_account->balance = bcadd($to_account->balance, $amount, 2);
        $service_account->balance = bcadd($service_account, $fee_amount,2);

        if ($from_account->balance < 0) {
            throw new Exception('Transfer is not possible, because "from" account balance is too small.');
        }

        $this->db->trans_begin();

        try {
            $this->update($from_account->id, ['balance' => $from_account->balance]);
            $this->update($to_account->id, ['balance' => $to_account->balance]);
            $this->update($service_account->id,['balance' => $service_account->balance]);

            $this->db->insert(
                'transfer',
                [
                    'from_id' => $from_account->id,
                    'to_id' => $to_account->id,
                    'from_balance' => $from_account->balance,
                    'to_balance' => $to_account->balance,
                    'amount' => $amount,
                    'time' => date('Y-m-d H:i:s')
                ]
            );
            $this->db->trans_commit();
        } catch (Exception $e) {
            $this->db->trans_rollback();
            throw new Exception($e->getMessage());
        }

    }

}


