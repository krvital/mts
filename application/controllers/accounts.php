<?php


/**
 * Class Main
 * @property Account_model $account
 */
class Accounts extends Controller
{
    protected $menu = [
        'accounts' => ['url' => '/accounts/', 'label' => 'Accounts', 'active' => 'active'],
        'create' => ['url' => '/createAccount/', 'label' => 'Create', 'active' => ''],
        'transfer' => ['url' => '/transfer/', 'label' => 'Transfer', 'active' => ''],
    ];
    protected $current_page;

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Account_model', 'account');
        $this->load->library('session');
    }

    /**
     * List all accounts page
     */
    public function index()
    {
        $this->current_page = 'accounts';
        $this->render('accounts/index', ['accounts' => $this->account->get_all()]);

    }

    /**
     * Create account page
     */
    public function create()
    {
        $this->load->helper(['form', 'url']);
        $this->load->library('form_validation');

        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
        $this->form_validation->set_rules('client', 'Client', 'required|trim');
        $this->form_validation->set_rules('serial', 'Serial', 'required|is_unique[account.serial]|is_natural');
        $this->form_validation->set_rules('balance', 'Balance', 'required|numeric|callback_decimal_size');

        if ($this->form_validation->run()) {
            $this->account->client = $this->input->post('client');
            $this->account->serial = $this->input->post('serial');
            $this->account->balance = $this->input->post('balance');
            $this->account->insert();
            $this->session->set_flashdata('success', "Account {$this->account->client} was added");
            redirect('/accounts/');
        }
        $this->current_page = 'create';
        $this->render('accounts/create');
    }

    /**
     * List all transfers of account set by id
     * @param $id
     */
    public function view($id)
    {
        $account = $this->account->get($id);
        if (empty($account)) {
            show_404();
        }

        $this->current_page = 'accounts';
        $this->render('accounts/view', ['transfers' => $this->account->get_transfers($id)]);
    }


    /**
     * Transfer money between accounts
     */
    public function transfer()
    {
        $this->load->helper(['form', 'url']);
        $this->load->library(['form_validation', 'session']);

        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
        $this->form_validation->set_rules('from_id', 'From', 'required');
        $this->form_validation->set_rules('to_id', 'To', 'required|callback_not_matches[from_id]');
        $this->form_validation->set_rules('amount', 'Amount', 'required|numeric|callback_decimal_size');

        if ($this->form_validation->run()) {
            $from = $this->input->post('from_id');
            $to = $this->input->post('to_id');
            $amount = $this->input->post('amount');
            try {
                $this->account->transfer_money($from, $to, $amount);
                redirect('/accounts/');
            } catch (Exception $e) {
                $error = $e->getMessage();
            }
        }


        $accounts = $this->account->get_all();
        $this->current_page = 'transfer';
        $this->render(
            'transfer',
            [
                'from_accounts' => $accounts,
                'to_accounts' => $accounts,
                'error' => isset($error) ? $error : null
            ]
        );

    }


    public function decimal_size($str)
    {
        if (strpos($str, '.') === false) {
            return true;
        }
        list($whole, $decimal) = explode('.', $str);

        if ($decimal > 99) {
            $this->form_validation->set_message('decimal_size', 'Incorrect balance value. Expects number with not more than two decimals');
            return false;
        } else {
            return true;
        }
    }

    /**
     * Validator for transfer action which validate that two field not matches
     * @param $str
     * @param $field
     * @return bool
     */
    public function not_matches($str, $field)
    {
        if (!isset($_POST[$field])) {
            return true;
        }

        $field = $_POST[$field];
        if ($str === $field) {
            $this->form_validation->set_message('not_matches', 'You can not transfer money to the same account');
            return false;
        } else {
            return true;
        }
    }

    /**
     * Set active menu item
     * @param $name
     */
    protected function set_active_item($name)
    {
        foreach ($this->menu as $key => $item) {
            $this->menu[$key]['active'] = '';
        }
        if (isset($this->menu[$name])) {
            $this->menu[$name]['active'] = 'active';
        }
    }

} 