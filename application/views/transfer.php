<div id="create-account-form" class="well">
    <legend>Transfer money</legend>
    <?php echo form_open('transfer'); ?>
    <div class="error">
        <?php echo $error;?>
    </div>
    <div class="form-group">
        <?php echo form_error('from_id'); ?>
        <select name="from_id" class="form-control">
            <option value="">Select account FROM</option>
            <?php foreach ($from_accounts as $acc): ?>
                <option value="<?php echo $acc->id ?>" <?php echo set_select('from_id', $acc->id);?> >
                    <?php echo $acc->client . ' : ' . $acc->serial; ?>
                </option>
            <?php endforeach;; ?>
        </select>
    </div>
    <div class="form-group">
        <?php echo form_error('to_id'); ?>
        <select name="to_id" class="form-control">
            <option value="">Select account TO</option>
            <?php foreach ($from_accounts as $acc): ?>
                <option value="<?php echo $acc->id ?>" <?php echo set_select('to_id', $acc->id);?>>
                    <?php echo $acc->client . ' : ' . $acc->serial; ?>
                </option>
            <?php endforeach;; ?>
        </select>

    </div>
    <div class="form-group">
        <?php echo form_error('amount'); ?>
        <input type="number" step="0.01" class="form-control" name="amount" value="<?= set_value('amount') ?>"
               placeholder="Amount">
    </div>
    <button type="submit" class="btn btn-primary" style="width: 100%">Transfer</button>
    </form>
</div>
