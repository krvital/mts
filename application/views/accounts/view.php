<legend>Account transfers</legend>
<div class="transfers">
    <table class="table table-condensed table-bordered table-striped">
        <tr>
            <th>Datetime</th>
            <th style="text-align: center; width: 100px">Account</th>
            <th style="text-align: center; width: 100px">Income</th>
            <th style="text-align: center; width: 100px">Withdraw</th>
            <th style="text-align: center; width: 100px">Balance</th>
        </tr>
        <?php foreach ($transfers as $tr): ?>
            <tr>
                <td><?= $tr->time ?></td>
                <td style="text-align: center;"><?= $tr->serial ?></td>
                <td style="text-align: center;"><?= $tr->income ?></td>
                <td style="text-align: center;"><?= $tr->withdraw ?></td>
                <td style="text-align: center;"><?= $tr->balance ?></td>
            </tr>
        <?php endforeach; ?>
    </table>
</div>
<a href="/accounts/" class="btn btn-default">&#8592; Back to accounts</a>
<br><br>