<legend>Accounts</legend>
<div class="accounts">
    <?php if($this->session->flashdata('success')):?>
    <div class="alert alert-success"><?=$this->session->flashdata('success')?></div>
    <?php endif;?>
    <table class="table table-condensed table-bordered table-striped">
        <tr>
            <th>Client</th>
            <th style="width: 150px">Serial</th>
            <th style="width: 150px">Balance</th>
        </tr>
        <?php foreach ($accounts as $acc): ?>
            <tr>
                <td><a href="/accounts/<?=$acc->id?>"><?= $acc->client ?></a></td>
                <td style="text-align: center"><?= $acc->serial ?></td>
                <td style="text-align: center"><?= round($acc->balance,2) ?></td>
            </tr>
        <?php endforeach; ?>
    </table>
</div>
