<div id="create-account-form" class="well">
    <legend>Create account</legend>
    <?php echo form_open(''); ?>
    <div class="form-group">
        <?php echo form_error('client');?>
        <input type="text" class="form-control" name="client" value="<?= set_value('client') ?>"
               placeholder="Client name">
    </div>
    <div class="form-group">
        <?php echo form_error('serial');?>
        <input type="text" class="form-control" name="serial" value="<?= set_value('serial') ?>"
               placeholder="Serial number">
    </div>
    <div class="form-group">
        <?php echo form_error('balance');?>
        <input type="text" class="form-control" name="balance" value="<?= set_value('balance') ?>"
               placeholder="Initial balance">
    </div>
    <button type="submit" class="btn btn-primary" style="width: 100%">Create</button>
    </form>
</div>
