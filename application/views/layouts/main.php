<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>MONEY TRANSFER SYSTEM</title>

    <link href="/css/bootstrap.min.css" rel="stylesheet">
    <link href="/css/jumbotron-narrow.css" rel="stylesheet">
    <link href="/css/main.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>

<div class="container">
    <div class="header">
        <ul class="nav nav-pills pull-right">
            <?php foreach($menu as $item):?>
                <li class="<?=$item['active']?>"><a href="<?=$item['url']?>"><?=$item['label']?></a></li>
            <?php endforeach;?>
        </ul>
        <h3 class="text-muted">MONEY TRANSFER SYSTEM</h3>
    </div>

    <?php echo $content; ?>

    <div class="footer">
        <p>&copy; Money transfer system 2014</p>
    </div>

</div>
<!-- /container -->

</body>
</html>
