<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>PAGE NOT FOUND</title>

    <link href="/css/bootstrap.min.css" rel="stylesheet">
    <link href="/css/jumbotron-narrow.css" rel="stylesheet">
    <link href="/css/main.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>

<div class="container">
    <div class="header">
        <ul class="nav nav-pills pull-right">
            <li class="active"><a href="/accounts/">Accounts</a></li>
            <li><a href="/createAccount/">Create</a></li>
            <li><a href="/transfer/">Transfer</a></li>
        </ul>
        <h3 class="text-muted">MONEY TRANSFER SYSTEM</h3>
    </div>

    <div style="text-align: center; margin: 50px">
        <h3><?php echo $heading;?></h3>
        The page you requested was not found.
    </div>


    <div class="footer">
        <p>&copy; Money transfer system 2014</p>
    </div>

</div>
<!-- /container -->

</body>
</html>
